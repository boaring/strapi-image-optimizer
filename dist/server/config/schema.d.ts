declare const configSchema: import("yup/lib/object").OptionalObjectSchema<{
    additionalResolutions: import("yup").ArraySchema<import("yup").NumberSchema<number, Record<string, any>, number>, import("yup/lib/types").AnyObject, number[], number[]>;
    exclude: import("yup").ArraySchema<import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>, import("yup/lib/types").AnyObject, any[], any[]>;
    formats: import("yup").ArraySchema<import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>, import("yup/lib/types").AnyObject, any[], any[]>;
    include: import("yup").ArraySchema<import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>, import("yup/lib/types").AnyObject, any[], any[]>;
    sizes: import("yup").ArraySchema<import("yup/lib/object").OptionalObjectSchema<{
        name: import("yup").StringSchema<string, Record<string, any>, string>;
        width: import("yup").NumberSchema<number, Record<string, any>, number>;
        height: import("yup").NumberSchema<number, Record<string, any>, number>;
        fit: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        position: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        withoutEnlargement: import("yup").BooleanSchema<boolean, Record<string, any>, boolean>;
    }, Record<string, any>, import("yup/lib/object").TypeOfShape<{
        name: import("yup").StringSchema<string, Record<string, any>, string>;
        width: import("yup").NumberSchema<number, Record<string, any>, number>;
        height: import("yup").NumberSchema<number, Record<string, any>, number>;
        fit: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        position: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        withoutEnlargement: import("yup").BooleanSchema<boolean, Record<string, any>, boolean>;
    }>>, import("yup/lib/types").AnyObject, import("yup/lib/object").TypeOfShape<{
        name: import("yup").StringSchema<string, Record<string, any>, string>;
        width: import("yup").NumberSchema<number, Record<string, any>, number>;
        height: import("yup").NumberSchema<number, Record<string, any>, number>;
        fit: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        position: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        withoutEnlargement: import("yup").BooleanSchema<boolean, Record<string, any>, boolean>;
    }>[], import("yup/lib/object").AssertsShape<{
        name: import("yup").StringSchema<string, Record<string, any>, string>;
        width: import("yup").NumberSchema<number, Record<string, any>, number>;
        height: import("yup").NumberSchema<number, Record<string, any>, number>;
        fit: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        position: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        withoutEnlargement: import("yup").BooleanSchema<boolean, Record<string, any>, boolean>;
    }>[]>;
    quality: import("yup").NumberSchema<number, Record<string, any>, number>;
}, Record<string, any>, import("yup/lib/object").TypeOfShape<{
    additionalResolutions: import("yup").ArraySchema<import("yup").NumberSchema<number, Record<string, any>, number>, import("yup/lib/types").AnyObject, number[], number[]>;
    exclude: import("yup").ArraySchema<import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>, import("yup/lib/types").AnyObject, any[], any[]>;
    formats: import("yup").ArraySchema<import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>, import("yup/lib/types").AnyObject, any[], any[]>;
    include: import("yup").ArraySchema<import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>, import("yup/lib/types").AnyObject, any[], any[]>;
    sizes: import("yup").ArraySchema<import("yup/lib/object").OptionalObjectSchema<{
        name: import("yup").StringSchema<string, Record<string, any>, string>;
        width: import("yup").NumberSchema<number, Record<string, any>, number>;
        height: import("yup").NumberSchema<number, Record<string, any>, number>;
        fit: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        position: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        withoutEnlargement: import("yup").BooleanSchema<boolean, Record<string, any>, boolean>;
    }, Record<string, any>, import("yup/lib/object").TypeOfShape<{
        name: import("yup").StringSchema<string, Record<string, any>, string>;
        width: import("yup").NumberSchema<number, Record<string, any>, number>;
        height: import("yup").NumberSchema<number, Record<string, any>, number>;
        fit: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        position: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        withoutEnlargement: import("yup").BooleanSchema<boolean, Record<string, any>, boolean>;
    }>>, import("yup/lib/types").AnyObject, import("yup/lib/object").TypeOfShape<{
        name: import("yup").StringSchema<string, Record<string, any>, string>;
        width: import("yup").NumberSchema<number, Record<string, any>, number>;
        height: import("yup").NumberSchema<number, Record<string, any>, number>;
        fit: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        position: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        withoutEnlargement: import("yup").BooleanSchema<boolean, Record<string, any>, boolean>;
    }>[], import("yup/lib/object").AssertsShape<{
        name: import("yup").StringSchema<string, Record<string, any>, string>;
        width: import("yup").NumberSchema<number, Record<string, any>, number>;
        height: import("yup").NumberSchema<number, Record<string, any>, number>;
        fit: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        position: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
        withoutEnlargement: import("yup").BooleanSchema<boolean, Record<string, any>, boolean>;
    }>[]>;
    quality: import("yup").NumberSchema<number, Record<string, any>, number>;
}>>;
export default configSchema;
