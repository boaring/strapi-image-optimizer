import type { Config } from "../models";
declare const _default: ({ strapi }: {
    strapi: any;
}) => {
    readonly settings: Config;
};
export default _default;
