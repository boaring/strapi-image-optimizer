"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const image_optimizer_service_1 = __importDefault(require("./image-optimizer-service"));
exports.default = {
    imageOptimizerService: image_optimizer_service_1.default,
};
