"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fs_1 = require("node:fs");
const node_path_1 = require("node:path");
const node_stream_1 = require("node:stream");
const p_limit_1 = __importDefault(require("p-limit"));
const sharp_1 = __importDefault(require("sharp"));
const pluginId_1 = __importDefault(require("../utils/pluginId"));
const strapi_server_1 = __importDefault(require("@strapi/upload/strapi-server"));
// biome-ignore lint/suspicious/noExplicitAny: <explanation>
const imageManipulation = (0, strapi_server_1.default)().services['image-manipulation'];
const utils_1 = require("@strapi/utils");
const { bytesToKbytes } = utils_1.file;
const defaultFormats = ["original", "webp", "avif"];
const defaultInclude = ["jpeg", "jpg", "png"];
const defaultQuality = 80;
let limitFetch = undefined;
const _optimizeImage = ({ strapi }) => async function optimizeImage(file) {
    // Get config
    const { exclude = [], formats = defaultFormats, include = defaultInclude, sizes, additionalResolutions, quality = defaultQuality, remoteURL = undefined, maxConcurrentFetches = 8, } = strapi.config.get(`plugin::${pluginId_1.default}`).settings;
    if (!limitFetch)
        limitFetch = (0, p_limit_1.default)(maxConcurrentFetches);
    const sourceFileType = file.ext.replace(".", "");
    if (exclude.includes(sourceFileType.toLowerCase()) ||
        !include.includes(sourceFileType.toLowerCase())) {
        return Promise.all([]);
    }
    const imageFormatPromises = [];
    let buffer = undefined;
    const getBuffer = (async () => await (buffer !== null && buffer !== void 0 ? buffer : (buffer = new Promise((resolve, reject) => {
        let chunks = [];
        const stream = file.getStream();
        stream.on('data', (chunk) => { chunks.push(chunk); });
        stream.once('end', () => {
            resolve(Buffer.concat(chunks));
        });
        stream.once('error', reject);
    }))));
    const optimizedCache = {};
    formats.forEach((format) => {
        console.log();
        sizes.forEach((size) => {
            imageFormatPromises.push(generateImage(file, getBuffer, optimizedCache, remoteURL, format, size, quality));
            if (additionalResolutions) {
                additionalResolutions.forEach((resizeFactor) => {
                    imageFormatPromises.push(generateImage(file, getBuffer, optimizedCache, remoteURL, format, size, quality, resizeFactor));
                });
            }
        });
    });
    return Promise.all(imageFormatPromises);
};
async function generateImage(sourceFile, getBuffer, optimizedCache, remoteURL, format, size, quality, resizeFactor = 1) {
    const resizeFactorPart = resizeFactor === 1 ? "" : `_${resizeFactor}x`;
    const sizeName = `${size.name}${resizeFactorPart}`;
    const formatPart = format === "original" ? "" : `_${format}`;
    return {
        key: `${sizeName}${formatPart}`,
        file: !!remoteURL ? await resizeFileWithLambda(remoteURL, await getBuffer(), optimizedCache, sourceFile, sizeName, format, size, quality, resizeFactor) : await resizeFileTo(optimizedCache, sourceFile, sizeName, format, size, quality, resizeFactor),
    };
}
async function resizeFileWithLambda(remoteURL, buffer, optimizedCache, sourceFile, sizeName, format, size, quality, resizeFactor) {
    const originalSize = !size.width && !size.height;
    let { width, height } = originalSize || (size.withoutEnlargement && ((size.width && size.width > sourceFile.width) || (size.height && size.height > sourceFile.height)))
        ? { width: sourceFile.width, height: sourceFile.height }
        : { width: size.width, height: size.height };
    width = width ? width * resizeFactor : undefined;
    height = height ? height * resizeFactor : undefined;
    const url = `${remoteURL}?f=${format}&q=${quality}${!!width ? '&w=' + width : (!!height ? '&h=' + height : '')}`;
    let blob;
    let metadata;
    if (!optimizedCache[url]) {
        optimizedCache[url] = limitFetch(() => fetch(url, {
            method: 'POST',
            body: buffer,
            headers: {
                'Content-Type': `image/${sourceFile.ext.replace('.', '')}`
            }
        })).then(async (res) => {
            if (res.headers.get('content-type') === 'application/json' || res.headers.get('content-type').startsWith('text/plain')) {
                throw new Error(await res.text());
            }
            const blob = await res.blob();
            const metadata = await getMetadata(node_stream_1.Duplex.from(blob.stream()));
            return { blob, metadata };
        });
    }
    ({ blob, metadata } = await optimizedCache[url]);
    const imageHash = `${sizeName}_${format}_${sourceFile.hash}`;
    const filePath = (0, node_path_1.join)(sourceFile.tmpWorkingDirectory, imageHash);
    await writeStreamToFile(node_stream_1.Duplex.from(blob.stream()), filePath);
    return {
        name: getFileName(sourceFile, sizeName),
        hash: imageHash,
        ext: getFileExtension(sourceFile, format),
        mime: getFileMimeType(sourceFile, format),
        path: sourceFile.path,
        width: metadata.width,
        height: metadata.height,
        size: metadata.size && bytesToKbytes(metadata.size),
        getStream: () => (0, node_fs_1.createReadStream)(filePath),
    };
}
async function resizeFileTo(optimizedCache, sourceFile, sizeName, format, size, quality, resizeFactor) {
    let sharpInstance = (0, sharp_1.default)();
    //sharpInstance = sharpAddFormatSettings(sharpInstance, { quality });
    if (format !== "original") {
        const options = { quality, progressive: undefined, compressionLevel: undefined };
        if (format === 'jpeg' || format === 'jpg') {
            options.progressive = true;
        }
        else if (format === 'png') {
            options.progressive = true;
            options.compressionLevel = Math.floor(((quality !== null && quality !== void 0 ? quality : 100) / 100) * 9);
            delete options.quality;
        }
        sharpInstance = sharpInstance.toFormat(format, { quality });
    }
    sharpInstance = sharpAddResizeSettings(sharpInstance, size, resizeFactor, sourceFile);
    const imageHash = `${sizeName}_${format}_${sourceFile.hash}`;
    const filePath = (0, node_path_1.join)(sourceFile.tmpWorkingDirectory, imageHash);
    const newImageStream = sourceFile.getStream().pipe(sharpInstance);
    await writeStreamToFile(newImageStream, filePath);
    const metadata = await getMetadata((0, node_fs_1.createReadStream)(filePath));
    return {
        name: getFileName(sourceFile, sizeName),
        hash: imageHash,
        ext: getFileExtension(sourceFile, format),
        mime: getFileMimeType(sourceFile, format),
        path: sourceFile.path,
        width: metadata.width,
        height: metadata.height,
        size: metadata.size && bytesToKbytes(metadata.size),
        getStream: () => (0, node_fs_1.createReadStream)(filePath),
    };
}
function sharpAddFormatSettings(sharpInstance, { quality }) {
    // TODO: Add jxl when it's no longer experimental
    return sharpInstance
        .jpeg({ quality, progressive: true, force: false })
        .png({
        compressionLevel: Math.floor(((quality !== null && quality !== void 0 ? quality : 100) / 100) * 9),
        progressive: true,
        force: false,
    })
        .webp({ quality, force: false })
        .avif({ quality, force: false })
        .heif({ quality, force: false })
        .tiff({ quality, force: false });
}
function sharpAddResizeSettings(sharpInstance, size, factor, sourceFile) {
    const originalSize = !size.width && !size.height;
    const { width, height } = originalSize
        ? { width: sourceFile.width, height: sourceFile.height }
        : { width: size.width, height: size.height };
    return sharpInstance.resize({
        width: width ? width * factor : undefined,
        height: height ? height * factor : undefined,
        fit: size.fit,
        // Position "center" cannot be set since it's the default (see: https://sharp.pixelplumbing.com/api-resize#resize).
        position: size.position === "center" ? undefined : size.position,
        withoutEnlargement: size.withoutEnlargement,
    });
}
async function writeStreamToFile(sharpsStream, path) {
    return new Promise((resolve, reject) => {
        const writeStream = (0, node_fs_1.createWriteStream)(path);
        // Reject promise if there is an error with the provided stream
        sharpsStream.on("error", reject);
        sharpsStream.pipe(writeStream);
        writeStream.on("close", resolve);
        writeStream.on("error", reject);
    });
}
async function getMetadata(readStream) {
    return new Promise((resolve, reject) => {
        const sharpInstance = (0, sharp_1.default)();
        sharpInstance.metadata().then(resolve).catch(reject);
        (0, node_stream_1.pipeline)(readStream, sharpInstance, (err) => {
            if (err) {
                reject(err);
            }
        });
    });
}
function getFileName(sourceFile, sizeName) {
    const fileNameWithoutExtension = sourceFile.name.replace(/\.[^\/.]+$/, "");
    return `${sizeName}_${fileNameWithoutExtension}`;
}
function getFileExtension(sourceFile, format) {
    return format === "original" ? sourceFile.ext : `.${format}`;
}
function getFileMimeType(sourceFile, format) {
    return format === "original" ? sourceFile.mime : `image/${format}`;
}
const imageOptimizerService = ({ strapi }) => ({
    generateResponsiveFormats: _optimizeImage({ strapi }),
    ...imageManipulation,
});
exports.default = imageOptimizerService;
