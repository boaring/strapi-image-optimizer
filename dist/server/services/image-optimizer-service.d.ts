declare const imageOptimizerService: ({ strapi }: {
    strapi: any;
}) => any;
export default imageOptimizerService;
export type DefaultType = typeof imageOptimizerService;
