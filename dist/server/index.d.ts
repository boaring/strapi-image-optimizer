declare const _default: {
    config: {
        default: {};
        validator(config: import("./models").Config): Promise<void>;
    };
    services: {
        imageOptimizerService: ({ strapi }: {
            strapi: any;
        }) => any;
    };
};
export default _default;
