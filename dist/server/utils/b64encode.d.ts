import { Transform } from 'node:stream';
/**
 * Transforms a Buffer stream of binary data to a stream of Base64 text. Note that this will
 * also work on a stream of pure strings, as the Writeable base class will automatically decode
 * text string chunks into Buffers.
 * You can pass optionally a line length or a prefix
 * @extends Transform
 */
export default class Base64Encode extends Transform {
    extra: Buffer;
    /**
     * Creates a Base64Encode
     * @param {Object=} options - Options for stream creation. Passed to Transform constructor as-is.
     * @param {string=} options.inputEncoding - The input chunk format. Default is 'utf8'. No effect on Buffer input chunks.
     * @param {string=} options.outputEncoding - The output chunk format. Default is 'utf8'. Pass `null` for Buffer chunks.
     * @param {number=} options.lineLength - The max line-length of the output stream.
     * @param {string=} options.prefix - Prefix for output string.
     */
    constructor(options: any);
    /**
    * Transforms a Buffer chunk of data to a Base64 string chunk.
    * @param {Buffer} chunk
    * @param {string} encoding - unused since chunk is always a Buffer
    * @param cb
    * @private
    */
    _transform(chunk: any, encoding: any, cb: any): void;
    /**
     * Emits 0 or 4 extra characters of Base64 data.
     * @param cb
     * @private
     */
    _flush(cb: any): void;
}
