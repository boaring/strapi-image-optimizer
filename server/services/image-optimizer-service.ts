import { type ReadStream, createReadStream, createWriteStream } from "node:fs";
import { join } from "node:path";
import { Duplex, type Readable, pipeline } from 'node:stream';
import plimit from "p-limit";
import sharp, { type Metadata, type Sharp } from "sharp";
import pluginId from "../utils/pluginId";

import pluginUpload from '@strapi/upload/strapi-server';
// biome-ignore lint/suspicious/noExplicitAny: <explanation>
const imageManipulation = pluginUpload().services['image-manipulation'] as any;

import { file } from "@strapi/utils";
const { bytesToKbytes } = file;

import type {
  File,
  ImageSize,
  OutputFormat,
  SourceFile,
  SourceFormat,
  StrapiImageFormat,
} from "../models";

const defaultFormats: OutputFormat[] = ["original", "webp", "avif"];
const defaultInclude: SourceFormat[] = ["jpeg", "jpg", "png"];
const defaultQuality = 80;
let limitFetch: (fn: ()=>Promise<any>)=>Promise<any> = undefined;

type OptimizedCache = {[key: string]: Promise<{
  blob: Blob,
  metadata: Metadata,
}>};
const _optimizeImage = ({strapi})=>
async function optimizeImage(file: SourceFile): Promise<StrapiImageFormat[]> {
  
  // Get config
  const {
    exclude = [],
    formats = defaultFormats,
    include = defaultInclude,
    sizes,
    additionalResolutions,
    quality = defaultQuality,
    remoteURL = undefined,
    maxConcurrentFetches = 8,
  } = strapi.config.get(`plugin::${pluginId}`).settings;

  if (!limitFetch)
    limitFetch = plimit(maxConcurrentFetches);

  const sourceFileType = file.ext.replace(".", "");
  if (
    exclude.includes(sourceFileType.toLowerCase() as SourceFormat) ||
    !include.includes(sourceFileType.toLowerCase() as SourceFormat)
  ) {
    return Promise.all([]);
  }

  const imageFormatPromises: Promise<StrapiImageFormat>[] = [];
  let buffer: Promise<Buffer> = undefined;
  const getBuffer = (async () => await (buffer ?? (buffer = new Promise((resolve, reject)=>{
    let chunks = [];
    const stream = file.getStream();
    stream.on('data', (chunk) => { chunks.push(chunk); });
    stream.once('end', ()=>{
      resolve(Buffer.concat(chunks));
    });
    stream.once('error', reject)
  }))));

  const optimizedCache: OptimizedCache = {};

  formats.forEach((format) => {
    console.log()
    sizes.forEach((size) => {
      imageFormatPromises.push(generateImage(file, getBuffer, optimizedCache, remoteURL, format, size, quality));
      if (additionalResolutions) {
        additionalResolutions.forEach((resizeFactor) => {
          imageFormatPromises.push(
            generateImage(file, getBuffer, optimizedCache, remoteURL, format, size, quality, resizeFactor)
          );
        });
      }
    });
  });

  return Promise.all(imageFormatPromises);
}

async function generateImage(
  sourceFile: SourceFile,
  getBuffer: ()=>Promise<Buffer>,
  optimizedCache: OptimizedCache,
  remoteURL: string,
  format: OutputFormat,
  size: ImageSize,
  quality: number,
  resizeFactor = 1
): Promise<StrapiImageFormat> {
  const resizeFactorPart = resizeFactor === 1 ? "" : `_${resizeFactor}x`;
  const sizeName = `${size.name}${resizeFactorPart}`;
  const formatPart = format === "original" ? "" : `_${format}`;
  return {
    key: `${sizeName}${formatPart}`,
    file: !!remoteURL?await resizeFileWithLambda(
      remoteURL,
      await getBuffer(),
      optimizedCache,
      sourceFile,
      sizeName,
      format,
      size,
      quality,
      resizeFactor
    ):await resizeFileTo(
      optimizedCache,
      sourceFile,
      sizeName,
      format,
      size,
      quality,
      resizeFactor
    ),
  };
}

async function resizeFileWithLambda(remoteURL: string, buffer: Buffer, optimizedCache: OptimizedCache, sourceFile: SourceFile, sizeName: string, format: OutputFormat, size: ImageSize, quality: number, resizeFactor: number): Promise<File> {
  const originalSize = !size.width && !size.height;
  let { width, height } = originalSize || (size.withoutEnlargement && ((size.width && size.width>sourceFile.width) || (size.height && size.height>sourceFile.height)))
    ? { width: sourceFile.width, height: sourceFile.height }
    : { width: size.width, height: size.height };

  width = width ? width * resizeFactor : undefined;
  height = height ? height * resizeFactor : undefined;
  const url = `${remoteURL}?f=${format}&q=${quality}${!!width?'&w='+width:(!!height?'&h='+height:'')}`;
  let blob: Blob;
  let metadata: Metadata;
  if (!optimizedCache[url]) {
    optimizedCache[url] = limitFetch(()=>fetch(url, {
      method: 'POST',
      body: buffer,
      headers: {
        'Content-Type': `image/${sourceFile.ext.replace('.', '')}`
      }
    })).then(async res=>{
      if (res.headers.get('content-type') === 'application/json' || res.headers.get('content-type').startsWith('text/plain')){
        throw new Error(await res.text());
      }
      const blob = await res.blob();
      const metadata = await getMetadata(Duplex.from(blob.stream()));
      return {blob, metadata};
    });
  }
  ({blob, metadata} = await optimizedCache[url]);

  const imageHash = `${sizeName}_${format}_${sourceFile.hash}`;
  const filePath = join(sourceFile.tmpWorkingDirectory, imageHash);
  await writeStreamToFile(Duplex.from(blob.stream()), filePath);
  return {
    name: getFileName(sourceFile, sizeName),
    hash: imageHash,
    ext: getFileExtension(sourceFile, format),
    mime: getFileMimeType(sourceFile, format),
    path: sourceFile.path,
    width: metadata.width,
    height: metadata.height,
    size: metadata.size && bytesToKbytes(metadata.size),
    getStream: () => createReadStream(filePath),
  };
}

async function resizeFileTo(
  optimizedCache: OptimizedCache,
  sourceFile: SourceFile,
  sizeName: string,
  format: OutputFormat,
  size: ImageSize,
  quality: number,
  resizeFactor: number
): Promise<File> {
  let sharpInstance: Sharp = sharp();
  //sharpInstance = sharpAddFormatSettings(sharpInstance, { quality });
  if (format !== "original") {
    const options = {quality, progressive: undefined, compressionLevel: undefined};
    if (format === 'jpeg' || format === 'jpg') {
      options.progressive = true;
    } else if (format === 'png') {
      options.progressive = true;
      options.compressionLevel = Math.floor(((quality??100) / 100) * 9);
      delete options.quality;
    }
    sharpInstance = sharpInstance.toFormat(format, {quality});
  }
  sharpInstance = sharpAddResizeSettings(
    sharpInstance,
    size,
    resizeFactor,
    sourceFile
  );

  const imageHash = `${sizeName}_${format}_${sourceFile.hash}`;
  const filePath = join(sourceFile.tmpWorkingDirectory, imageHash);
  const newImageStream = sourceFile.getStream().pipe(sharpInstance);
  await writeStreamToFile(newImageStream, filePath);

  const metadata = await getMetadata(createReadStream(filePath));
  return {
    name: getFileName(sourceFile, sizeName),
    hash: imageHash,
    ext: getFileExtension(sourceFile, format),
    mime: getFileMimeType(sourceFile, format),
    path: sourceFile.path,
    width: metadata.width,
    height: metadata.height,
    size: metadata.size && bytesToKbytes(metadata.size),
    getStream: () => createReadStream(filePath),
  };
}

function sharpAddFormatSettings(
  sharpInstance: Sharp,
  { quality }: { quality?: number }
): Sharp {
  // TODO: Add jxl when it's no longer experimental
  return sharpInstance
    .jpeg({ quality, progressive: true, force: false })
    .png({
      compressionLevel: Math.floor(((quality ?? 100) / 100) * 9),
      progressive: true,
      force: false,
    })
    .webp({ quality, force: false })
    .avif({ quality, force: false })
    .heif({ quality, force: false })
    .tiff({ quality, force: false });
}

function sharpAddResizeSettings(
  sharpInstance: Sharp,
  size: ImageSize,
  factor: number,
  sourceFile: SourceFile
): Sharp {
  const originalSize = !size.width && !size.height;
  const { width, height } = originalSize
    ? { width: sourceFile.width, height: sourceFile.height }
    : { width: size.width, height: size.height };

  return sharpInstance.resize({
    width: width ? width * factor : undefined,
    height: height ? height * factor : undefined,
    fit: size.fit,
    // Position "center" cannot be set since it's the default (see: https://sharp.pixelplumbing.com/api-resize#resize).
    position: size.position === "center" ? undefined : size.position,
    withoutEnlargement: size.withoutEnlargement,
  });
}

async function writeStreamToFile(sharpsStream: Readable, path: string) {
  return new Promise((resolve, reject) => {
    const writeStream = createWriteStream(path);
    // Reject promise if there is an error with the provided stream
    sharpsStream.on("error", reject);
    sharpsStream.pipe(writeStream);
    writeStream.on("close", resolve);
    writeStream.on("error", reject);
  });
}

async function getMetadata(readStream: ReadStream|Readable): Promise<Metadata> {
  return new Promise((resolve, reject) => {
    const sharpInstance = sharp();
    sharpInstance.metadata().then(resolve).catch(reject);
    pipeline(readStream, sharpInstance, (err) => {
      if (err) {
        reject(err);
      }
    });
  });
}

function getFileName(sourceFile: File, sizeName: string) {
  const fileNameWithoutExtension = sourceFile.name.replace(/\.[^\/.]+$/, "");
  return `${sizeName}_${fileNameWithoutExtension}`;
}

function getFileExtension(sourceFile: File, format: OutputFormat) {
  return format === "original" ? sourceFile.ext : `.${format}`;
}

function getFileMimeType(sourceFile: File, format: OutputFormat) {
  return format === "original" ? sourceFile.mime : `image/${format}`;
}

const imageOptimizerService = ({strapi}) => ({
  generateResponsiveFormats: _optimizeImage({strapi}),
  ...imageManipulation,
});

export default imageOptimizerService;

export type DefaultType = typeof imageOptimizerService;