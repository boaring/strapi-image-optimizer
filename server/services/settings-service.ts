import type { Config } from "../models";
import pluginId from "../utils/pluginId";

export default ({strapi})=>({
  get settings(): Config {
    return strapi.config.get(`plugin::${pluginId}`);
  }
});
